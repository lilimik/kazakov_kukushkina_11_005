from typing import Any
from aiohttp import ClientSession, ClientTimeout


class HttpAdapter():
    async def http_session(
        self,
        method: str,
        url: str,
        **kwargs: Any,
    ) -> str:
        async with ClientSession(timeout=ClientTimeout(60)) as client:
            async with client.request(method, url, **kwargs) as response:
                return await response.text()
