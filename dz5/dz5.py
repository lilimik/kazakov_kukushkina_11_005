from flask import Flask, request, render_template
from sklearn.metrics.pairwise import cosine_similarity
import re
from collections import defaultdict
from sklearn.feature_extraction.text import TfidfVectorizer
import pandas as pd
from bs4 import BeautifulSoup


app = Flask(__name__)


def extract_text(html) -> str:
    soup = BeautifulSoup(html, 'html.parser')
    text = soup.get_text()
    tokens = " ".join(re.findall(r'\b(?:[А-Я][а-я]+|[а-я]{3,})\b', text))
    return tokens


def get_params():
    data = defaultdict(list)
    with open('D:\\Projects_Python\\infopoisk\\1dz\\dz1\\index.txt', 'r', encoding='utf-8') as file_with_indexes:
        i = 0
        while (line := file_with_indexes.readline()) and i < 100:
            i += 1
            file_index, file_link = line.split(', ')
            with open(f'D:\\Projects_Python\\infopoisk\\1dz\\dz1\\scraped\\{file_index}.txt', 'r', encoding='utf-8') as file_html:
                text = extract_text(file_html)
            data['link'].append(file_link.replace('\n', ''))
            data['text'].append(text)

    df = pd.DataFrame(data=data)

    vectorizer = TfidfVectorizer()
    X = vectorizer.fit_transform(df['text'])
    return df, X, vectorizer


df, tf_idf, vectorizer = get_params()


@app.route("/")
def search():
    print(request.args)
    query = request.args.get('query', '')
    result = []

    if query:
        query_vec = vectorizer.transform([query])
        results = cosine_similarity(tf_idf, query_vec).reshape((-1,))
        for i in results.argsort()[-10:][::-1]:
            result.append(df.iloc[i, 0])

    return render_template('search.html', result=result)


if __name__ == '__main__':
    app.run('127.0.0.1', 9992, debug=True)
