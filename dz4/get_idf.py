import math
import os
from collections import defaultdict
from typing import List, Set, Dict


INVERTED_LEMMATIZED_TOKENS_FILE = 'inverted_lemmatized_tokens'
FILE_ENCODING = 'utf-8'
LEMMATIZED_TOKENS_FILE = '../dz2/lemmatized_tokens.txt'
TOKENS_FILE = '../dz2/tokens.txt'
SCRAPED_RESULTS_FOLDER = '../dz1/scraped'
COMMON_RESULTS_FILE = '../dz1/выкачка.txt'


def check_file(
        file_number: int,
        file: str,
        tokens: Dict[str, List[str]],
        lem_tokens: Dict[str, List[str]],
        tokens_data: Dict[str, Set[int]],
        lem_data: Dict[str, Set[int]]
):
    for key, values in lem_tokens.items():
        for value in values:
            if value in file:
                lem_data[key].add(file_number)

    for key, values in tokens.items():
        for value in values:
            if value in file:
                tokens_data[key].add(file_number)


def check_filename(filename: str) -> str | None:
    file_number = filename[:-4]
    return file_number if filename.endswith('.txt') and file_number.isdigit() else None


def get_idf() -> tuple[dict[str, float], dict[str, float]]:
    tokens: dict[str, list[str]] = defaultdict(list)
    tokens_data: Dict[str, set[int]] = defaultdict(set)
    lem_tokens: dict[str, list[str]] = defaultdict(list)
    lem_data: Dict[str, set[int]] = defaultdict(set)
    lem_result: dict[str, float] = dict()
    tokens_result: dict[str, float] = dict()

    with open(TOKENS_FILE, mode='r', encoding=FILE_ENCODING) as f:
        line = f.readline()
        while line:
            value = line.replace('\n', '')
            tokens[value] += value
            line = f.readline()

    with open(LEMMATIZED_TOKENS_FILE, mode='r', encoding=FILE_ENCODING) as f:
        line = f.readline()
        while line:
            key, values = line.split(' ', 1)
            values = values.replace('\n', '').split(' ')
            lem_tokens[key] += values
            line = f.readline()

    files_count = 0
    for filename in os.listdir(SCRAPED_RESULTS_FOLDER):
        if file_number := check_filename(filename):
            files_count += 1
            file_path = os.path.join(SCRAPED_RESULTS_FOLDER, filename)
            with open(file_path, mode='r', encoding="utf-8") as f:
                check_file(int(file_number), f.read(), tokens, lem_tokens, tokens_data, lem_data)

    for key, values in lem_data.items():
        lem_result[key] = math.log2(round(files_count / len(values), 10))
    for key, values in tokens_data.items():
        tokens_result[key] = math.log2(round(files_count / len(values), 10))

    return tokens_result, lem_result
