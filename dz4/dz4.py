import asyncio
import os
import re
from collections import defaultdict

from bs4 import BeautifulSoup

from dz4.get_idf import get_idf


def extract_tokens(html) -> list[str]:
    soup = BeautifulSoup(html, 'html.parser')
    text = soup.get_text()
    tokens = re.findall(r'\b(?:[А-ЯЁ][а-яё]+|[а-яё]{3,})\b', text)
    return tokens


def check_file(
        file: str,
        tokens: dict[str, list[str]],
        lem_tokens: dict[str, list[str]],
        file_number: str,
        tokens_idf: dict[str, float],
        lem_tokens_idf: dict[str, float]
):
    tokens_list = extract_tokens(file)

    with open(f'{RESULT_LEM_FOLDER}/{file_number}.txt', 'w', encoding=FILE_ENCODING) as f:
        for key, values in lem_tokens.items():
            try:
                tokens_in_file = set(values).intersection(set(tokens_list))
                token_occurrences_count = 0
                for token in tokens_in_file:
                    token_occurrences_count += tokens_list.count(token)
                tf = round(token_occurrences_count / len(tokens_list), 10)
                tf_idf = lem_tokens_idf[key] * tf
                f.write(f'{key} {lem_tokens_idf[key]} {tf_idf}\n')
            except KeyError:
                continue

    with open(os.path.join(RESULT_TOKENS_FOLDER, f'{file_number}.txt'), 'w', encoding=FILE_ENCODING) as f:
        for key, values in tokens.items():
            try:
                tokens_in_file = set(values).intersection(set(tokens_list))
                token_occurrences_count = 0
                for token in tokens_in_file:
                    token_occurrences_count += tokens_list.count(token)
                tf = round(token_occurrences_count / len(tokens_list), 10)
                tf_idf = lem_tokens_idf[key] * tf
                f.write(f'{key} {tokens_idf[key]} {tf_idf}\n')
            except KeyError:
                continue

def check_filename(filename: str) -> str | None:
    file_number = filename[:-4]
    return file_number if filename.endswith('.txt') and file_number.isdigit() else None


async def main():
    result = get_idf()
    tokens_idf: dict[str, float] = result[0]
    lem_tokens_idf: dict[str, float] = result[1]

    # print(f'{tokens_idf=}')
    # print(f'{lem_tokens_idf=}')

    lem_tokens: dict[str, list[str]] = defaultdict(list)
    tokens: dict[str, list[str]] = defaultdict(list)

    with open(TOKENS_FILE, mode='r', encoding=FILE_ENCODING) as f:
        line = f.readline()
        while line:
            value = line.replace('\n', '')
            tokens[value].append(value)
            line = f.readline()

    with open(LEMMATIZED_TOKENS_FILE, mode='r', encoding=FILE_ENCODING) as f:
        line = f.readline()
        while line:
            key, values = line.split(' ', 1)
            values = values.replace('\n', '').split(' ')
            lem_tokens[key] += values
            line = f.readline()

    # print(f'{tokens=}')
    # print(f'{lem_tokens=}')

    for filename in os.listdir(SCRAPED_RESULTS_FOLDER):
        if file_number := check_filename(filename):
            file_path = os.path.join(SCRAPED_RESULTS_FOLDER, filename)
            with open(file_path, mode='r', encoding="utf-8") as f:
                check_file(f.read(), dict(tokens), dict(lem_tokens), file_number, tokens_idf, lem_tokens_idf)


if __name__ == '__main__':
    RESULT_TOKENS_FOLDER = 'tokens_result'
    RESULT_LEM_FOLDER = 'lem_result'

    INVERTED_LEMMATIZED_TOKENS_FILE = 'inverted_lemmatized_tokens'
    FILE_ENCODING = 'utf-8'
    LEMMATIZED_TOKENS_FILE = '../dz2/lemmatized_tokens.txt'
    TOKENS_FILE = '../dz2/tokens.txt'
    SCRAPED_RESULTS_FOLDER = '../dz1/scraped'
    COMMON_RESULTS_FILE = '../dz1/выкачка.txt'
    asyncio.run(main())
