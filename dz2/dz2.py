import os
import re
from collections import defaultdict
from bs4 import BeautifulSoup
import pymorphy2


def extract_tokens(html) -> set[str]:
    soup = BeautifulSoup(html, 'html.parser')
    text = soup.get_text()

    # Используем регулярное выражение для разбиения текста на токены (слова)
    tokens = set(re.findall(r'\b(?:[А-ЯЁ][а-яё]+|[а-яё]{3,})\b', text))

    return tokens


def grouping_by_lemmas(tokens: list[str]) -> dict[str, list[str]]:
    # Создание экземпляра морфологического анализатора
    morph = pymorphy2.MorphAnalyzer()

    lemmatized_tokens = defaultdict(list)

    for token in tokens:
        lemmatized_token = morph.parse(token)[0].normal_form
        lemmatized_tokens[lemmatized_token].append(token)

    return dict(lemmatized_tokens)


def main():
    tokens = set()
    for filename in os.listdir(SCRAPED_RESULTS_FOLDER):
        if filename.endswith('.txt') and filename[:-4].isdigit():
            file_path = os.path.join(SCRAPED_RESULTS_FOLDER, filename)
            with open(file_path, mode='r', encoding="utf-8") as f:
                tokens = tokens.union(extract_tokens(f.read()))

    lemmatized_tokens = grouping_by_lemmas(list(tokens))

    print(tokens)

    with open(TOKENS_FILE, mode='w', encoding="utf-8") as f:
        for token in tokens:
            f.write(f'{token}\n')

    with open(LEMMATIZED_TOKENS_FILE, mode='w', encoding="utf-8") as f:
        for key, values in lemmatized_tokens.items():
            f.write(f'{key} {" ".join(values)}\n')


if __name__ == '__main__':
    TOKENS_FILE = 'tokens.txt'
    LEMMATIZED_TOKENS_FILE = 'lemmatized_tokens.txt'
    SCRAPED_RESULTS_FOLDER = '../dz1/scraped'
    main()
