<!DOCTYPE html>
<html class="client-nojs" lang="ru" dir="ltr">
<head>
<meta charset="UTF-8"/>
<title>Аристов, Дмитрий Васильевич — FA100</title>
<script>document.documentElement.className = document.documentElement.className.replace( /(^|\s)client-nojs(\s|$)/, "$1client-js$2" );</script>
<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":0,"wgPageName":"Аристов,_Дмитрий_Васильевич","wgTitle":"Аристов, Дмитрий Васильевич","wgCurRevisionId":20677,"wgRevisionId":20677,"wgArticleId":10545,"wgIsArticle":true,"wgIsRedirect":false,"wgAction":"view","wgUserName":null,"wgUserGroups":["*"],"wgCategories":["Выпускники МФИ-ФА","Выпускники 2005 года","Персоналии по алфавиту","Ссылки на статьи в Википедии"],"wgBreakFrames":false,"wgPageContentLanguage":"ru","wgPageContentModel":"wikitext","wgSeparatorTransformTable":[",\t."," \t,"],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","январь","февраль","март","апрель","май","июнь","июль","август","сентябрь","октябрь","ноябрь","декабрь"],"wgMonthNamesShort":["","янв","фев","мар","апр","май","июн","июл","авг","сен","окт","ноя","дек"],"wgRelevantPageName":"Аристов,_Дмитрий_Васильевич","wgRelevantArticleId":10545,"wgRequestId":"c27fcb8ee1c34b08e79a245a","wgIsProbablyEditable":false,"wgRestrictionEdit":[],"wgRestrictionMove":[],"wgWikiEditorEnabledModules":{"toolbar":false,"dialogs":false,"preview":false,"publish":false}});mw.loader.state({"site.styles":"ready","noscript":"ready","user.styles":"ready","user":"ready","user.options":"loading","user.tokens":"loading","mediawiki.legacy.shared":"ready","mediawiki.legacy.commonPrint":"ready","mediawiki.sectionAnchor":"ready","mediawiki.skinning.interface":"ready","skins.vector.styles":"ready"});mw.loader.implement("user.options@0aplg68",function($,jQuery,require,module){mw.user.options.set({"variant":"ru"});});mw.loader.implement("user.tokens@1c916s5",function ( $, jQuery, require, module ) {
mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\","csrfToken":"+\\"});/*@nomin*/;

});mw.loader.load(["mediawiki.action.view.postEdit","site","mediawiki.page.startup","mediawiki.user","mediawiki.hidpi","mediawiki.page.ready","mediawiki.searchSuggest","skins.vector.js"]);});</script>
<link rel="stylesheet" href="/load.php?debug=false&amp;lang=ru&amp;modules=mediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.sectionAnchor%7Cmediawiki.skinning.interface%7Cskins.vector.styles&amp;only=styles&amp;skin=vector"/>
<script async="" src="/load.php?debug=false&amp;lang=ru&amp;modules=startup&amp;only=scripts&amp;skin=vector"></script>
<meta name="ResourceLoaderDynamicStyles" content=""/>
<meta name="generator" content="MediaWiki 1.29.1"/>
<link rel="shortcut icon" href="/favicon.ico"/>
<link rel="search" type="application/opensearchdescription+xml" href="/opensearch_desc.php" title="FA100 (ru)"/>
<link rel="EditURI" type="application/rsd+xml" href="http://wiki.fa100.ru/api.php?action=rsd"/>
<link rel="alternate" type="application/atom+xml" title="FA100 — Atom-лента" href="/index.php?title=%D0%A1%D0%BB%D1%83%D0%B6%D0%B5%D0%B1%D0%BD%D0%B0%D1%8F:%D0%A1%D0%B2%D0%B5%D0%B6%D0%B8%D0%B5_%D0%BF%D1%80%D0%B0%D0%B2%D0%BA%D0%B8&amp;feed=atom"/>
</head>
<body class="mediawiki ltr sitedir-ltr mw-hide-empty-elt ns-0 ns-subject page-Аристов_Дмитрий_Васильевич rootpage-Аристов_Дмитрий_Васильевич skin-vector action-view">		<div id="mw-page-base" class="noprint"></div>
		<div id="mw-head-base" class="noprint"></div>
		<div id="content" class="mw-body" role="main">
			<a id="top"></a>

						<div class="mw-indicators mw-body-content">
</div>
			<h1 id="firstHeading" class="firstHeading" lang="ru">Аристов, Дмитрий Васильевич</h1>
									<div id="bodyContent" class="mw-body-content">
									<div id="siteSub">Материал из FA100</div>
								<div id="contentSub"></div>
												<div id="jump-to-nav" class="mw-jump">
					Перейти к:					<a href="#mw-head">навигация</a>, 					<a href="#p-search">поиск</a>
				</div>
				<div id="mw-content-text" lang="ru" dir="ltr" class="mw-content-ltr"><p><b>Аристов Дмитрий Васильевич</b> (род. 1972) — российский юрист, директор Федеральной службы судебных приставов.
</p><p>Родился в городе Соликамске Пермской области 12 января 1972 года.
</p><p>В 1989 году окончил Киевское суворовское военное училище.
</p><p>Затем окончил Ярославское высшее военное финансовое училище имени генерала армии А. В. Хрулёва (1993), Военный финансово-экономический университет (2005) и МГИМО МИД России (2009).
</p><p>С 1989 по 2008 годы служил в Вооружённых Силах СССР и РФ, уволился в запас в звании полковника.
</p><p>В 2008—2009 годах занимал должность начальника отдела управления в Главном управлении Генеральной прокуратуры РФ.
</p><p>В 2009—2010 годах был заместителем директора Департамента регистрации ведомственных нормативных правовых актов, а в 2010—2012 годах — директором Департамента нормативно-правового регулирования, анализа и контроля в сфере исполнения уголовных наказаний и судебных актов Министерства юстиции РФ[2].
</p><p>В 2012—2017 годах служил заместителем министра юстиции РФ Александра Коновалова.
</p><p>20 марта 2017 года указом № 118 Президента РФ был назначен директором Федеральной службы судебных приставов — главным судебным приставом РФ[3].
</p>
<dl><dt>Государственные награды и звания</dt></dl>
<ul><li> Орден Почёта (Россия)</li>
<li> Полковник запаса (2008)</li>
<li> Действительный государственный советник Российской Федерации 1 класса (указ Президент РФ от 19.03.2013)</li>
<li> Почётная грамота Президента Российской Федерации (2015)</li>
<li> Генерал-полковник внутренней службы (указ Президента РФ № 2 от 01.01.2020)</li></ul>
<hr />
<p><a rel="nofollow" class="external text" href="https://ru.wikipedia.org/wiki/Аристов,_Дмитрий_Васильевич">Аристов Дмитрий Васильевич в русской Википедии</a>
</p>
<hr />
<div class="floatright"><a href="http://fa100.ru" rel="nofollow"><img alt="Logo Finance Academy.jpg" src="/images/thumb/2/21/Logo_Finance_Academy.jpg/60px-Logo_Finance_Academy.jpg" width="60" height="60" srcset="/images/thumb/2/21/Logo_Finance_Academy.jpg/90px-Logo_Finance_Academy.jpg 1.5x, /images/thumb/2/21/Logo_Finance_Academy.jpg/120px-Logo_Finance_Academy.jpg 2x" /></a></div>
<p><a rel="nofollow" class="external text" href="http://fa100.ru/person/89525"><b>Аристов Дмитрий Васильевич</b><br /> на сайте «Годы и Люди»</a>
</p>
<!-- 
NewPP limit report
Cached time: 20240404080539
Cache expiry: 86400
Dynamic content: false
CPU time usage: 0.004 seconds
Real time usage: 0.005 seconds
Preprocessor visited node count: 1/1000000
Preprocessor generated node count: 4/1000000
Post‐expand include size: 0/2097152 bytes
Template argument size: 0/2097152 bytes
Highest expansion depth: 1/40
Expensive parser function count: 0/100
-->
<!--
Transclusion expansion time report (%,ms,calls,template)
100.00%    0.000      1 -total
-->

<!-- Saved in parser cache with key wikidb:pcache:idhash:10545-0!*!*!*!*!5!* and timestamp 20240404080539 and revision id 20677
 -->
</div>					<div class="printfooter">
						Источник — «<a dir="ltr" href="http://wiki.fa100.ru/index.php?title=Аристов,_Дмитрий_Васильевич&amp;oldid=20677">http://wiki.fa100.ru/index.php?title=Аристов,_Дмитрий_Васильевич&amp;oldid=20677</a>»					</div>
				<div id="catlinks" class="catlinks" data-mw="interface"><div id="mw-normal-catlinks" class="mw-normal-catlinks"><a href="/index.php?title=%D0%A1%D0%BB%D1%83%D0%B6%D0%B5%D0%B1%D0%BD%D0%B0%D1%8F:%D0%9A%D0%B0%D1%82%D0%B5%D0%B3%D0%BE%D1%80%D0%B8%D0%B8" title="Служебная:Категории">Категории</a>: <ul><li><a href="/index.php?title=%D0%9A%D0%B0%D1%82%D0%B5%D0%B3%D0%BE%D1%80%D0%B8%D1%8F:%D0%92%D1%8B%D0%BF%D1%83%D1%81%D0%BA%D0%BD%D0%B8%D0%BA%D0%B8_%D0%9C%D0%A4%D0%98-%D0%A4%D0%90" title="Категория:Выпускники МФИ-ФА">Выпускники МФИ-ФА</a></li><li><a href="/index.php?title=%D0%9A%D0%B0%D1%82%D0%B5%D0%B3%D0%BE%D1%80%D0%B8%D1%8F:%D0%92%D1%8B%D0%BF%D1%83%D1%81%D0%BA%D0%BD%D0%B8%D0%BA%D0%B8_2005_%D0%B3%D0%BE%D0%B4%D0%B0" title="Категория:Выпускники 2005 года">Выпускники 2005 года</a></li><li><a href="/index.php?title=%D0%9A%D0%B0%D1%82%D0%B5%D0%B3%D0%BE%D1%80%D0%B8%D1%8F:%D0%9F%D0%B5%D1%80%D1%81%D0%BE%D0%BD%D0%B0%D0%BB%D0%B8%D0%B8_%D0%BF%D0%BE_%D0%B0%D0%BB%D1%84%D0%B0%D0%B2%D0%B8%D1%82%D1%83" title="Категория:Персоналии по алфавиту">Персоналии по алфавиту</a></li><li><a href="/index.php?title=%D0%9A%D0%B0%D1%82%D0%B5%D0%B3%D0%BE%D1%80%D0%B8%D1%8F:%D0%A1%D1%81%D1%8B%D0%BB%D0%BA%D0%B8_%D0%BD%D0%B0_%D1%81%D1%82%D0%B0%D1%82%D1%8C%D0%B8_%D0%B2_%D0%92%D0%B8%D0%BA%D0%B8%D0%BF%D0%B5%D0%B4%D0%B8%D0%B8" title="Категория:Ссылки на статьи в Википедии">Ссылки на статьи в Википедии</a></li></ul></div></div>				<div class="visualClear"></div>
							</div>
		</div>
		<div id="mw-navigation">
			<h2>Навигация</h2>

			<div id="mw-head">
									<div id="p-personal" role="navigation" class="" aria-labelledby="p-personal-label">
						<h3 id="p-personal-label">Персональные инструменты</h3>
						<ul>
							<li id="pt-login"><a href="/index.php?title=%D0%A1%D0%BB%D1%83%D0%B6%D0%B5%D0%B1%D0%BD%D0%B0%D1%8F:%D0%92%D1%85%D0%BE%D0%B4&amp;returnto=%D0%90%D1%80%D0%B8%D1%81%D1%82%D0%BE%D0%B2%2C+%D0%94%D0%BC%D0%B8%D1%82%D1%80%D0%B8%D0%B9+%D0%92%D0%B0%D1%81%D0%B8%D0%BB%D1%8C%D0%B5%D0%B2%D0%B8%D1%87" title="Здесь можно зарегистрироваться в системе, но это необязательно. [o]" accesskey="o">Войти</a></li>						</ul>
					</div>
									<div id="left-navigation">
										<div id="p-namespaces" role="navigation" class="vectorTabs" aria-labelledby="p-namespaces-label">
						<h3 id="p-namespaces-label">Пространства имён</h3>
						<ul>
															<li  id="ca-nstab-main" class="selected"><span><a href="/index.php?title=%D0%90%D1%80%D0%B8%D1%81%D1%82%D0%BE%D0%B2,_%D0%94%D0%BC%D0%B8%D1%82%D1%80%D0%B8%D0%B9_%D0%92%D0%B0%D1%81%D0%B8%D0%BB%D1%8C%D0%B5%D0%B2%D0%B8%D1%87"  title="Просмотр основной страницы [c]" accesskey="c">Статья</a></span></li>
															<li  id="ca-talk" class="new"><span><a href="/index.php?title=%D0%9E%D0%B1%D1%81%D1%83%D0%B6%D0%B4%D0%B5%D0%BD%D0%B8%D0%B5:%D0%90%D1%80%D0%B8%D1%81%D1%82%D0%BE%D0%B2,_%D0%94%D0%BC%D0%B8%D1%82%D1%80%D0%B8%D0%B9_%D0%92%D0%B0%D1%81%D0%B8%D0%BB%D1%8C%D0%B5%D0%B2%D0%B8%D1%87&amp;action=edit&amp;redlink=1"  title="Обсуждение основной страницы [t]" accesskey="t" rel="discussion">Обсуждение</a></span></li>
													</ul>
					</div>
										<div id="p-variants" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-variants-label">
												<h3 id="p-variants-label">
							<span>Варианты</span><a href="#"></a>
						</h3>

						<div class="menu">
							<ul>
															</ul>
						</div>
					</div>
									</div>
				<div id="right-navigation">
										<div id="p-views" role="navigation" class="vectorTabs" aria-labelledby="p-views-label">
						<h3 id="p-views-label">Просмотры</h3>
						<ul>
															<li id="ca-view" class="selected"><span><a href="/index.php?title=%D0%90%D1%80%D0%B8%D1%81%D1%82%D0%BE%D0%B2,_%D0%94%D0%BC%D0%B8%D1%82%D1%80%D0%B8%D0%B9_%D0%92%D0%B0%D1%81%D0%B8%D0%BB%D1%8C%D0%B5%D0%B2%D0%B8%D1%87" >Читать</a></span></li>
															<li id="ca-viewsource"><span><a href="/index.php?title=%D0%90%D1%80%D0%B8%D1%81%D1%82%D0%BE%D0%B2,_%D0%94%D0%BC%D0%B8%D1%82%D1%80%D0%B8%D0%B9_%D0%92%D0%B0%D1%81%D0%B8%D0%BB%D1%8C%D0%B5%D0%B2%D0%B8%D1%87&amp;action=edit"  title="Эта страница защищена от изменений, но вы можете посмотреть и скопировать её исходный текст [e]" accesskey="e">Просмотр вики-текста</a></span></li>
															<li id="ca-history" class="collapsible"><span><a href="/index.php?title=%D0%90%D1%80%D0%B8%D1%81%D1%82%D0%BE%D0%B2,_%D0%94%D0%BC%D0%B8%D1%82%D1%80%D0%B8%D0%B9_%D0%92%D0%B0%D1%81%D0%B8%D0%BB%D1%8C%D0%B5%D0%B2%D0%B8%D1%87&amp;action=history"  title="Журнал изменений страницы [h]" accesskey="h">История</a></span></li>
													</ul>
					</div>
										<div id="p-cactions" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-cactions-label">
						<h3 id="p-cactions-label"><span>Ещё</span><a href="#"></a></h3>

						<div class="menu">
							<ul>
															</ul>
						</div>
					</div>
										<div id="p-search" role="search">
						<h3>
							<label for="searchInput">Поиск</label>
						</h3>

						<form action="/index.php" id="searchform">
							<div id="simpleSearch">
							<input type="search" name="search" placeholder="Искать в FA100" title="Искать в FA100 [f]" accesskey="f" id="searchInput"/><input type="hidden" value="Служебная:Поиск" name="title"/><input type="submit" name="fulltext" value="Найти" title="Найти страницы, содержащие указанный текст" id="mw-searchButton" class="searchButton mw-fallbackSearchButton"/><input type="submit" name="go" value="Перейти" title="Перейти к странице, имеющей в точности такое название" id="searchButton" class="searchButton"/>							</div>
						</form>
					</div>
									</div>
			</div>
			<div id="mw-panel">
				<div id="p-logo" role="banner"><a class="mw-wiki-logo" href="/index.php?title=%D0%97%D0%B0%D0%B3%D0%BB%D0%B0%D0%B2%D0%BD%D0%B0%D1%8F_%D1%81%D1%82%D1%80%D0%B0%D0%BD%D0%B8%D1%86%D0%B0"  title="Перейти на заглавную страницу"></a></div>
						<div class="portal" role="navigation" id='p-navigation' aria-labelledby='p-navigation-label'>
			<h3 id='p-navigation-label'>Навигация</h3>

			<div class="body">
									<ul>
						<li id="n-mainpage-description"><a href="/index.php?title=%D0%97%D0%B0%D0%B3%D0%BB%D0%B0%D0%B2%D0%BD%D0%B0%D1%8F_%D1%81%D1%82%D1%80%D0%B0%D0%BD%D0%B8%D1%86%D0%B0" title="Перейти на заглавную страницу [z]" accesskey="z">Заглавная страница</a></li><li id="n-recentchanges"><a href="/index.php?title=%D0%A1%D0%BB%D1%83%D0%B6%D0%B5%D0%B1%D0%BD%D0%B0%D1%8F:%D0%A1%D0%B2%D0%B5%D0%B6%D0%B8%D0%B5_%D0%BF%D1%80%D0%B0%D0%B2%D0%BA%D0%B8" title="Список последних изменений [r]" accesskey="r">Свежие правки</a></li><li id="n-randompage"><a href="/index.php?title=%D0%A1%D0%BB%D1%83%D0%B6%D0%B5%D0%B1%D0%BD%D0%B0%D1%8F:%D0%A1%D0%BB%D1%83%D1%87%D0%B0%D0%B9%D0%BD%D0%B0%D1%8F_%D1%81%D1%82%D1%80%D0%B0%D0%BD%D0%B8%D1%86%D0%B0" title="Посмотреть случайно выбранную страницу [x]" accesskey="x">Случайная статья</a></li><li id="n-help"><a href="https://www.mediawiki.org/wiki/Special:MyLanguage/Help:Contents" title="Место, где можно получить справку">Справка</a></li>					</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id='p-tb' aria-labelledby='p-tb-label'>
			<h3 id='p-tb-label'>Инструменты</h3>

			<div class="body">
									<ul>
						<li id="t-whatlinkshere"><a href="/index.php?title=%D0%A1%D0%BB%D1%83%D0%B6%D0%B5%D0%B1%D0%BD%D0%B0%D1%8F:%D0%A1%D1%81%D1%8B%D0%BB%D0%BA%D0%B8_%D1%81%D1%8E%D0%B4%D0%B0/%D0%90%D1%80%D0%B8%D1%81%D1%82%D0%BE%D0%B2,_%D0%94%D0%BC%D0%B8%D1%82%D1%80%D0%B8%D0%B9_%D0%92%D0%B0%D1%81%D0%B8%D0%BB%D1%8C%D0%B5%D0%B2%D0%B8%D1%87" title="Список всех страниц, ссылающихся на данную [j]" accesskey="j">Ссылки сюда</a></li><li id="t-recentchangeslinked"><a href="/index.php?title=%D0%A1%D0%BB%D1%83%D0%B6%D0%B5%D0%B1%D0%BD%D0%B0%D1%8F:%D0%A1%D0%B2%D1%8F%D0%B7%D0%B0%D0%BD%D0%BD%D1%8B%D0%B5_%D0%BF%D1%80%D0%B0%D0%B2%D0%BA%D0%B8/%D0%90%D1%80%D0%B8%D1%81%D1%82%D0%BE%D0%B2,_%D0%94%D0%BC%D0%B8%D1%82%D1%80%D0%B8%D0%B9_%D0%92%D0%B0%D1%81%D0%B8%D0%BB%D1%8C%D0%B5%D0%B2%D0%B8%D1%87" rel="nofollow" title="Последние изменения в страницах, на которые ссылается эта страница [k]" accesskey="k">Связанные правки</a></li><li id="t-specialpages"><a href="/index.php?title=%D0%A1%D0%BB%D1%83%D0%B6%D0%B5%D0%B1%D0%BD%D0%B0%D1%8F:%D0%A1%D0%BF%D0%B5%D1%86%D1%81%D1%82%D1%80%D0%B0%D0%BD%D0%B8%D1%86%D1%8B" title="Список служебных страниц [q]" accesskey="q">Спецстраницы</a></li><li id="t-print"><a href="/index.php?title=%D0%90%D1%80%D0%B8%D1%81%D1%82%D0%BE%D0%B2,_%D0%94%D0%BC%D0%B8%D1%82%D1%80%D0%B8%D0%B9_%D0%92%D0%B0%D1%81%D0%B8%D0%BB%D1%8C%D0%B5%D0%B2%D0%B8%D1%87&amp;printable=yes" rel="alternate" title="Версия этой страницы для печати [p]" accesskey="p">Версия для печати</a></li><li id="t-permalink"><a href="/index.php?title=%D0%90%D1%80%D0%B8%D1%81%D1%82%D0%BE%D0%B2,_%D0%94%D0%BC%D0%B8%D1%82%D1%80%D0%B8%D0%B9_%D0%92%D0%B0%D1%81%D0%B8%D0%BB%D1%8C%D0%B5%D0%B2%D0%B8%D1%87&amp;oldid=20677" title="Постоянная ссылка на эту версию страницы">Постоянная ссылка</a></li><li id="t-info"><a href="/index.php?title=%D0%90%D1%80%D0%B8%D1%81%D1%82%D0%BE%D0%B2,_%D0%94%D0%BC%D0%B8%D1%82%D1%80%D0%B8%D0%B9_%D0%92%D0%B0%D1%81%D0%B8%D0%BB%D1%8C%D0%B5%D0%B2%D0%B8%D1%87&amp;action=info" title="Подробнее об этой странице">Сведения о странице</a></li><li id="t-cite"><a href="/index.php?title=%D0%A1%D0%BB%D1%83%D0%B6%D0%B5%D0%B1%D0%BD%D0%B0%D1%8F:%D0%A6%D0%B8%D1%82%D0%B0%D1%82%D0%B0&amp;page=%D0%90%D1%80%D0%B8%D1%81%D1%82%D0%BE%D0%B2%2C_%D0%94%D0%BC%D0%B8%D1%82%D1%80%D0%B8%D0%B9_%D0%92%D0%B0%D1%81%D0%B8%D0%BB%D1%8C%D0%B5%D0%B2%D0%B8%D1%87&amp;id=20677" title="Информация о том, как цитировать эту страницу">Цитировать страницу</a></li>					</ul>
							</div>
		</div>
				</div>
		</div>
		<div id="footer" role="contentinfo">
							<ul id="footer-info">
											<li id="footer-info-lastmod"> Эта страница последний раз была отредактирована 4 марта 2022 в 13:55.</li>
									</ul>
							<ul id="footer-places">
											<li id="footer-places-privacy"><a href="/index.php?title=FA100:%D0%9F%D0%BE%D0%BB%D0%B8%D1%82%D0%B8%D0%BA%D0%B0_%D0%BA%D0%BE%D0%BD%D1%84%D0%B8%D0%B4%D0%B5%D0%BD%D1%86%D0%B8%D0%B0%D0%BB%D1%8C%D0%BD%D0%BE%D1%81%D1%82%D0%B8" title="FA100:Политика конфиденциальности">Политика конфиденциальности</a></li>
											<li id="footer-places-about"><a href="/index.php?title=FA100:%D0%9E%D0%BF%D0%B8%D1%81%D0%B0%D0%BD%D0%B8%D0%B5" title="FA100:Описание">Описание FA100</a></li>
											<li id="footer-places-disclaimer"><a href="/index.php?title=FA100:%D0%9E%D1%82%D0%BA%D0%B0%D0%B7_%D0%BE%D1%82_%D0%BE%D1%82%D0%B2%D0%B5%D1%82%D1%81%D1%82%D0%B2%D0%B5%D0%BD%D0%BD%D0%BE%D1%81%D1%82%D0%B8" title="FA100:Отказ от ответственности">Отказ от ответственности</a></li>
									</ul>
										<ul id="footer-icons" class="noprint">
											<li id="footer-poweredbyico">
							<a href="//www.mediawiki.org/"><img src="/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" srcset="/resources/assets/poweredby_mediawiki_132x47.png 1.5x, /resources/assets/poweredby_mediawiki_176x62.png 2x" width="88" height="31"/></a>						</li>
									</ul>
						<div style="clear:both"></div>
		</div>
		<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgPageParseReport":{"limitreport":{"cputime":"0.004","walltime":"0.005","ppvisitednodes":{"value":1,"limit":1000000},"ppgeneratednodes":{"value":4,"limit":1000000},"postexpandincludesize":{"value":0,"limit":2097152},"templateargumentsize":{"value":0,"limit":2097152},"expansiondepth":{"value":1,"limit":40},"expensivefunctioncount":{"value":0,"limit":100},"timingprofile":["100.00%    0.000      1 -total"]},"cachereport":{"timestamp":"20240404080539","ttl":86400,"transientcontent":false}}});});</script><script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgBackendResponseTime":83});});</script>
	</body>
</html>
