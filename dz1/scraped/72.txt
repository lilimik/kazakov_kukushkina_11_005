<!DOCTYPE html>
<html class="client-nojs" lang="ru" dir="ltr">
<head>
<meta charset="UTF-8"/>
<title>Власов, Валентин Степанович — FA100</title>
<script>document.documentElement.className = document.documentElement.className.replace( /(^|\s)client-nojs(\s|$)/, "$1client-js$2" );</script>
<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":0,"wgPageName":"Власов,_Валентин_Степанович","wgTitle":"Власов, Валентин Степанович","wgCurRevisionId":20724,"wgRevisionId":20724,"wgArticleId":10871,"wgIsArticle":true,"wgIsRedirect":false,"wgAction":"view","wgUserName":null,"wgUserGroups":["*"],"wgCategories":["Выпускники МФИ-ФА","Выпускники 1994 года","Персоналии по алфавиту","Ссылки на статьи в Википедии"],"wgBreakFrames":false,"wgPageContentLanguage":"ru","wgPageContentModel":"wikitext","wgSeparatorTransformTable":[",\t."," \t,"],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","январь","февраль","март","апрель","май","июнь","июль","август","сентябрь","октябрь","ноябрь","декабрь"],"wgMonthNamesShort":["","янв","фев","мар","апр","май","июн","июл","авг","сен","окт","ноя","дек"],"wgRelevantPageName":"Власов,_Валентин_Степанович","wgRelevantArticleId":10871,"wgRequestId":"8ee97c15f86ca82e036c632f","wgIsProbablyEditable":false,"wgRestrictionEdit":[],"wgRestrictionMove":[],"wgWikiEditorEnabledModules":{"toolbar":false,"dialogs":false,"preview":false,"publish":false}});mw.loader.state({"site.styles":"ready","noscript":"ready","user.styles":"ready","user":"ready","user.options":"loading","user.tokens":"loading","mediawiki.legacy.shared":"ready","mediawiki.legacy.commonPrint":"ready","mediawiki.sectionAnchor":"ready","mediawiki.skinning.interface":"ready","skins.vector.styles":"ready"});mw.loader.implement("user.options@0aplg68",function($,jQuery,require,module){mw.user.options.set({"variant":"ru"});});mw.loader.implement("user.tokens@1c916s5",function ( $, jQuery, require, module ) {
mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\","csrfToken":"+\\"});/*@nomin*/;

});mw.loader.load(["mediawiki.action.view.postEdit","site","mediawiki.page.startup","mediawiki.user","mediawiki.hidpi","mediawiki.page.ready","mediawiki.searchSuggest","skins.vector.js"]);});</script>
<link rel="stylesheet" href="/load.php?debug=false&amp;lang=ru&amp;modules=mediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.sectionAnchor%7Cmediawiki.skinning.interface%7Cskins.vector.styles&amp;only=styles&amp;skin=vector"/>
<script async="" src="/load.php?debug=false&amp;lang=ru&amp;modules=startup&amp;only=scripts&amp;skin=vector"></script>
<meta name="ResourceLoaderDynamicStyles" content=""/>
<meta name="generator" content="MediaWiki 1.29.1"/>
<link rel="shortcut icon" href="/favicon.ico"/>
<link rel="search" type="application/opensearchdescription+xml" href="/opensearch_desc.php" title="FA100 (ru)"/>
<link rel="EditURI" type="application/rsd+xml" href="http://wiki.fa100.ru/api.php?action=rsd"/>
<link rel="alternate" type="application/atom+xml" title="FA100 — Atom-лента" href="/index.php?title=%D0%A1%D0%BB%D1%83%D0%B6%D0%B5%D0%B1%D0%BD%D0%B0%D1%8F:%D0%A1%D0%B2%D0%B5%D0%B6%D0%B8%D0%B5_%D0%BF%D1%80%D0%B0%D0%B2%D0%BA%D0%B8&amp;feed=atom"/>
</head>
<body class="mediawiki ltr sitedir-ltr mw-hide-empty-elt ns-0 ns-subject page-Власов_Валентин_Степанович rootpage-Власов_Валентин_Степанович skin-vector action-view">		<div id="mw-page-base" class="noprint"></div>
		<div id="mw-head-base" class="noprint"></div>
		<div id="content" class="mw-body" role="main">
			<a id="top"></a>

						<div class="mw-indicators mw-body-content">
</div>
			<h1 id="firstHeading" class="firstHeading" lang="ru">Власов, Валентин Степанович</h1>
									<div id="bodyContent" class="mw-body-content">
									<div id="siteSub">Материал из FA100</div>
								<div id="contentSub"></div>
												<div id="jump-to-nav" class="mw-jump">
					Перейти к:					<a href="#mw-head">навигация</a>, 					<a href="#p-search">поиск</a>
				</div>
				<div id="mw-content-text" lang="ru" dir="ltr" class="mw-content-ltr"><p><b>Власов Валентин Степанович</b> (1946—2020) — советский и российский государственный и политический деятель.
</p><p>Родился 20 августа 1946 в Архангельске. Детство провёл там же.
</p><p>Трудовую деятельность начал в 1963 году токарем завода «Красная кузница». Затем служил в рядах Советской армии. После демобилизации работал токарем в Мурманском морском пароходстве, мастером производственного обучения Архангельского областного штаба гражданской обороны, а впоследствии — руководителем НВП Архангельского торгового профучилища.
</p><p>С 1974 года занимал пост секретаря комитета ВЛКСМ, с 1976 года — секретаря партбюро Архангельского ПШО. В 1978 году заочно окончил Архангельский государственный пединститут имени М. В. Ломоносова. В 1979 году Власов был назначен завотделом Октябрьского райкома КПСС Архангельска. В 1980—1983 годах занимал должность заместителя председателя исполнительного комитета Октябрьского райсовета народных депутатов. В 1985 году Власов был направлен на работу в Узбекскую ССР, в 1985—1986 годах был вторым секретарём Маргиланского горкома КПСС, в 1986—1988 годах — первый секретарём Кувасайского горкома, в 1988—1990 годах — инструктором отдела ЦК Коммунистической партии Узбекской ССР и заведующим государственно-партийным отделом Ферганского обкома.
</p><p>В 1990 году вернулся в Архангельск. До 1996 года работал в родном городе, пройдя путь до мэра Архангельска и главы администрации Архангельской области. В 1994 году окончил Всероссийский заочный финансово-экономический институт.
</p><p>В 1996 году Власов был назначен первым заместителем Полномочного представителя Правительства Российской Федерации в Чеченской республике Ивана Рыбкина, а в 1997 году — полномочным представителем.
</p><p>В 1997 году окончил Российскую академию государственной службы при Президенте Российской Федерации по специальности «юриспруденция».
</p><p>24 июля 1999 — 14 сентября 1999 — и. о. главы республики Карачаево-Черкесия.
</p><p>15 марта 1999 — 10 ноября 2002 — член Центральной избирательной комиссии Российской Федерации, а с 24 марта 1999 — заместитель председателя Центральной избирательной комиссии Российской Федерации.
</p><p>5 августа 2002 — 27 февраля 2006 — Чрезвычайный и полномочный посол Российской Федерации в Республике Мальта.
</p><p>6 декабря 2006 — 30 июля 2012 — Чрезвычайный и полномочный посол Российской Федерации в Киргизии.
</p><p>Скончался после продолжительной болезни 5 июля 2020 года в Москве. Похоронен на Троекуровском кладбище в Москве.
</p>
<hr />
<p><a rel="nofollow" class="external text" href="https://ru.wikipedia.org/wiki/Власов,_Валентин_Степанович">Власов Валентин Степанович в русской Википедии</a>
</p>
<hr />
<div class="floatright"><a href="http://fa100.ru" rel="nofollow"><img alt="Logo Finance Academy.jpg" src="/images/thumb/2/21/Logo_Finance_Academy.jpg/60px-Logo_Finance_Academy.jpg" width="60" height="60" srcset="/images/thumb/2/21/Logo_Finance_Academy.jpg/90px-Logo_Finance_Academy.jpg 1.5x, /images/thumb/2/21/Logo_Finance_Academy.jpg/120px-Logo_Finance_Academy.jpg 2x" /></a></div>
<p><a rel="nofollow" class="external text" href="http://fa100.ru/person/94822"><b>Власов Валентин Степанович</b><br /> на сайте «Годы и Люди»</a>
</p>
<!-- 
NewPP limit report
Cached time: 20240404080851
Cache expiry: 86400
Dynamic content: false
CPU time usage: 0.005 seconds
Real time usage: 0.005 seconds
Preprocessor visited node count: 1/1000000
Preprocessor generated node count: 4/1000000
Post‐expand include size: 0/2097152 bytes
Template argument size: 0/2097152 bytes
Highest expansion depth: 1/40
Expensive parser function count: 0/100
-->
<!--
Transclusion expansion time report (%,ms,calls,template)
100.00%    0.000      1 -total
-->

<!-- Saved in parser cache with key wikidb:pcache:idhash:10871-0!*!*!*!*!5!* and timestamp 20240404080851 and revision id 20724
 -->
</div>					<div class="printfooter">
						Источник — «<a dir="ltr" href="http://wiki.fa100.ru/index.php?title=Власов,_Валентин_Степанович&amp;oldid=20724">http://wiki.fa100.ru/index.php?title=Власов,_Валентин_Степанович&amp;oldid=20724</a>»					</div>
				<div id="catlinks" class="catlinks" data-mw="interface"><div id="mw-normal-catlinks" class="mw-normal-catlinks"><a href="/index.php?title=%D0%A1%D0%BB%D1%83%D0%B6%D0%B5%D0%B1%D0%BD%D0%B0%D1%8F:%D0%9A%D0%B0%D1%82%D0%B5%D0%B3%D0%BE%D1%80%D0%B8%D0%B8" title="Служебная:Категории">Категории</a>: <ul><li><a href="/index.php?title=%D0%9A%D0%B0%D1%82%D0%B5%D0%B3%D0%BE%D1%80%D0%B8%D1%8F:%D0%92%D1%8B%D0%BF%D1%83%D1%81%D0%BA%D0%BD%D0%B8%D0%BA%D0%B8_%D0%9C%D0%A4%D0%98-%D0%A4%D0%90" title="Категория:Выпускники МФИ-ФА">Выпускники МФИ-ФА</a></li><li><a href="/index.php?title=%D0%9A%D0%B0%D1%82%D0%B5%D0%B3%D0%BE%D1%80%D0%B8%D1%8F:%D0%92%D1%8B%D0%BF%D1%83%D1%81%D0%BA%D0%BD%D0%B8%D0%BA%D0%B8_1994_%D0%B3%D0%BE%D0%B4%D0%B0" title="Категория:Выпускники 1994 года">Выпускники 1994 года</a></li><li><a href="/index.php?title=%D0%9A%D0%B0%D1%82%D0%B5%D0%B3%D0%BE%D1%80%D0%B8%D1%8F:%D0%9F%D0%B5%D1%80%D1%81%D0%BE%D0%BD%D0%B0%D0%BB%D0%B8%D0%B8_%D0%BF%D0%BE_%D0%B0%D0%BB%D1%84%D0%B0%D0%B2%D0%B8%D1%82%D1%83" title="Категория:Персоналии по алфавиту">Персоналии по алфавиту</a></li><li><a href="/index.php?title=%D0%9A%D0%B0%D1%82%D0%B5%D0%B3%D0%BE%D1%80%D0%B8%D1%8F:%D0%A1%D1%81%D1%8B%D0%BB%D0%BA%D0%B8_%D0%BD%D0%B0_%D1%81%D1%82%D0%B0%D1%82%D1%8C%D0%B8_%D0%B2_%D0%92%D0%B8%D0%BA%D0%B8%D0%BF%D0%B5%D0%B4%D0%B8%D0%B8" title="Категория:Ссылки на статьи в Википедии">Ссылки на статьи в Википедии</a></li></ul></div></div>				<div class="visualClear"></div>
							</div>
		</div>
		<div id="mw-navigation">
			<h2>Навигация</h2>

			<div id="mw-head">
									<div id="p-personal" role="navigation" class="" aria-labelledby="p-personal-label">
						<h3 id="p-personal-label">Персональные инструменты</h3>
						<ul>
							<li id="pt-login"><a href="/index.php?title=%D0%A1%D0%BB%D1%83%D0%B6%D0%B5%D0%B1%D0%BD%D0%B0%D1%8F:%D0%92%D1%85%D0%BE%D0%B4&amp;returnto=%D0%92%D0%BB%D0%B0%D1%81%D0%BE%D0%B2%2C+%D0%92%D0%B0%D0%BB%D0%B5%D0%BD%D1%82%D0%B8%D0%BD+%D0%A1%D1%82%D0%B5%D0%BF%D0%B0%D0%BD%D0%BE%D0%B2%D0%B8%D1%87" title="Здесь можно зарегистрироваться в системе, но это необязательно. [o]" accesskey="o">Войти</a></li>						</ul>
					</div>
									<div id="left-navigation">
										<div id="p-namespaces" role="navigation" class="vectorTabs" aria-labelledby="p-namespaces-label">
						<h3 id="p-namespaces-label">Пространства имён</h3>
						<ul>
															<li  id="ca-nstab-main" class="selected"><span><a href="/index.php?title=%D0%92%D0%BB%D0%B0%D1%81%D0%BE%D0%B2,_%D0%92%D0%B0%D0%BB%D0%B5%D0%BD%D1%82%D0%B8%D0%BD_%D0%A1%D1%82%D0%B5%D0%BF%D0%B0%D0%BD%D0%BE%D0%B2%D0%B8%D1%87"  title="Просмотр основной страницы [c]" accesskey="c">Статья</a></span></li>
															<li  id="ca-talk" class="new"><span><a href="/index.php?title=%D0%9E%D0%B1%D1%81%D1%83%D0%B6%D0%B4%D0%B5%D0%BD%D0%B8%D0%B5:%D0%92%D0%BB%D0%B0%D1%81%D0%BE%D0%B2,_%D0%92%D0%B0%D0%BB%D0%B5%D0%BD%D1%82%D0%B8%D0%BD_%D0%A1%D1%82%D0%B5%D0%BF%D0%B0%D0%BD%D0%BE%D0%B2%D0%B8%D1%87&amp;action=edit&amp;redlink=1"  title="Обсуждение основной страницы [t]" accesskey="t" rel="discussion">Обсуждение</a></span></li>
													</ul>
					</div>
										<div id="p-variants" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-variants-label">
												<h3 id="p-variants-label">
							<span>Варианты</span><a href="#"></a>
						</h3>

						<div class="menu">
							<ul>
															</ul>
						</div>
					</div>
									</div>
				<div id="right-navigation">
										<div id="p-views" role="navigation" class="vectorTabs" aria-labelledby="p-views-label">
						<h3 id="p-views-label">Просмотры</h3>
						<ul>
															<li id="ca-view" class="selected"><span><a href="/index.php?title=%D0%92%D0%BB%D0%B0%D1%81%D0%BE%D0%B2,_%D0%92%D0%B0%D0%BB%D0%B5%D0%BD%D1%82%D0%B8%D0%BD_%D0%A1%D1%82%D0%B5%D0%BF%D0%B0%D0%BD%D0%BE%D0%B2%D0%B8%D1%87" >Читать</a></span></li>
															<li id="ca-viewsource"><span><a href="/index.php?title=%D0%92%D0%BB%D0%B0%D1%81%D0%BE%D0%B2,_%D0%92%D0%B0%D0%BB%D0%B5%D0%BD%D1%82%D0%B8%D0%BD_%D0%A1%D1%82%D0%B5%D0%BF%D0%B0%D0%BD%D0%BE%D0%B2%D0%B8%D1%87&amp;action=edit"  title="Эта страница защищена от изменений, но вы можете посмотреть и скопировать её исходный текст [e]" accesskey="e">Просмотр вики-текста</a></span></li>
															<li id="ca-history" class="collapsible"><span><a href="/index.php?title=%D0%92%D0%BB%D0%B0%D1%81%D0%BE%D0%B2,_%D0%92%D0%B0%D0%BB%D0%B5%D0%BD%D1%82%D0%B8%D0%BD_%D0%A1%D1%82%D0%B5%D0%BF%D0%B0%D0%BD%D0%BE%D0%B2%D0%B8%D1%87&amp;action=history"  title="Журнал изменений страницы [h]" accesskey="h">История</a></span></li>
													</ul>
					</div>
										<div id="p-cactions" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-cactions-label">
						<h3 id="p-cactions-label"><span>Ещё</span><a href="#"></a></h3>

						<div class="menu">
							<ul>
															</ul>
						</div>
					</div>
										<div id="p-search" role="search">
						<h3>
							<label for="searchInput">Поиск</label>
						</h3>

						<form action="/index.php" id="searchform">
							<div id="simpleSearch">
							<input type="search" name="search" placeholder="Искать в FA100" title="Искать в FA100 [f]" accesskey="f" id="searchInput"/><input type="hidden" value="Служебная:Поиск" name="title"/><input type="submit" name="fulltext" value="Найти" title="Найти страницы, содержащие указанный текст" id="mw-searchButton" class="searchButton mw-fallbackSearchButton"/><input type="submit" name="go" value="Перейти" title="Перейти к странице, имеющей в точности такое название" id="searchButton" class="searchButton"/>							</div>
						</form>
					</div>
									</div>
			</div>
			<div id="mw-panel">
				<div id="p-logo" role="banner"><a class="mw-wiki-logo" href="/index.php?title=%D0%97%D0%B0%D0%B3%D0%BB%D0%B0%D0%B2%D0%BD%D0%B0%D1%8F_%D1%81%D1%82%D1%80%D0%B0%D0%BD%D0%B8%D1%86%D0%B0"  title="Перейти на заглавную страницу"></a></div>
						<div class="portal" role="navigation" id='p-navigation' aria-labelledby='p-navigation-label'>
			<h3 id='p-navigation-label'>Навигация</h3>

			<div class="body">
									<ul>
						<li id="n-mainpage-description"><a href="/index.php?title=%D0%97%D0%B0%D0%B3%D0%BB%D0%B0%D0%B2%D0%BD%D0%B0%D1%8F_%D1%81%D1%82%D1%80%D0%B0%D0%BD%D0%B8%D1%86%D0%B0" title="Перейти на заглавную страницу [z]" accesskey="z">Заглавная страница</a></li><li id="n-recentchanges"><a href="/index.php?title=%D0%A1%D0%BB%D1%83%D0%B6%D0%B5%D0%B1%D0%BD%D0%B0%D1%8F:%D0%A1%D0%B2%D0%B5%D0%B6%D0%B8%D0%B5_%D0%BF%D1%80%D0%B0%D0%B2%D0%BA%D0%B8" title="Список последних изменений [r]" accesskey="r">Свежие правки</a></li><li id="n-randompage"><a href="/index.php?title=%D0%A1%D0%BB%D1%83%D0%B6%D0%B5%D0%B1%D0%BD%D0%B0%D1%8F:%D0%A1%D0%BB%D1%83%D1%87%D0%B0%D0%B9%D0%BD%D0%B0%D1%8F_%D1%81%D1%82%D1%80%D0%B0%D0%BD%D0%B8%D1%86%D0%B0" title="Посмотреть случайно выбранную страницу [x]" accesskey="x">Случайная статья</a></li><li id="n-help"><a href="https://www.mediawiki.org/wiki/Special:MyLanguage/Help:Contents" title="Место, где можно получить справку">Справка</a></li>					</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id='p-tb' aria-labelledby='p-tb-label'>
			<h3 id='p-tb-label'>Инструменты</h3>

			<div class="body">
									<ul>
						<li id="t-whatlinkshere"><a href="/index.php?title=%D0%A1%D0%BB%D1%83%D0%B6%D0%B5%D0%B1%D0%BD%D0%B0%D1%8F:%D0%A1%D1%81%D1%8B%D0%BB%D0%BA%D0%B8_%D1%81%D1%8E%D0%B4%D0%B0/%D0%92%D0%BB%D0%B0%D1%81%D0%BE%D0%B2,_%D0%92%D0%B0%D0%BB%D0%B5%D0%BD%D1%82%D0%B8%D0%BD_%D0%A1%D1%82%D0%B5%D0%BF%D0%B0%D0%BD%D0%BE%D0%B2%D0%B8%D1%87" title="Список всех страниц, ссылающихся на данную [j]" accesskey="j">Ссылки сюда</a></li><li id="t-recentchangeslinked"><a href="/index.php?title=%D0%A1%D0%BB%D1%83%D0%B6%D0%B5%D0%B1%D0%BD%D0%B0%D1%8F:%D0%A1%D0%B2%D1%8F%D0%B7%D0%B0%D0%BD%D0%BD%D1%8B%D0%B5_%D0%BF%D1%80%D0%B0%D0%B2%D0%BA%D0%B8/%D0%92%D0%BB%D0%B0%D1%81%D0%BE%D0%B2,_%D0%92%D0%B0%D0%BB%D0%B5%D0%BD%D1%82%D0%B8%D0%BD_%D0%A1%D1%82%D0%B5%D0%BF%D0%B0%D0%BD%D0%BE%D0%B2%D0%B8%D1%87" rel="nofollow" title="Последние изменения в страницах, на которые ссылается эта страница [k]" accesskey="k">Связанные правки</a></li><li id="t-specialpages"><a href="/index.php?title=%D0%A1%D0%BB%D1%83%D0%B6%D0%B5%D0%B1%D0%BD%D0%B0%D1%8F:%D0%A1%D0%BF%D0%B5%D1%86%D1%81%D1%82%D1%80%D0%B0%D0%BD%D0%B8%D1%86%D1%8B" title="Список служебных страниц [q]" accesskey="q">Спецстраницы</a></li><li id="t-print"><a href="/index.php?title=%D0%92%D0%BB%D0%B0%D1%81%D0%BE%D0%B2,_%D0%92%D0%B0%D0%BB%D0%B5%D0%BD%D1%82%D0%B8%D0%BD_%D0%A1%D1%82%D0%B5%D0%BF%D0%B0%D0%BD%D0%BE%D0%B2%D0%B8%D1%87&amp;printable=yes" rel="alternate" title="Версия этой страницы для печати [p]" accesskey="p">Версия для печати</a></li><li id="t-permalink"><a href="/index.php?title=%D0%92%D0%BB%D0%B0%D1%81%D0%BE%D0%B2,_%D0%92%D0%B0%D0%BB%D0%B5%D0%BD%D1%82%D0%B8%D0%BD_%D0%A1%D1%82%D0%B5%D0%BF%D0%B0%D0%BD%D0%BE%D0%B2%D0%B8%D1%87&amp;oldid=20724" title="Постоянная ссылка на эту версию страницы">Постоянная ссылка</a></li><li id="t-info"><a href="/index.php?title=%D0%92%D0%BB%D0%B0%D1%81%D0%BE%D0%B2,_%D0%92%D0%B0%D0%BB%D0%B5%D0%BD%D1%82%D0%B8%D0%BD_%D0%A1%D1%82%D0%B5%D0%BF%D0%B0%D0%BD%D0%BE%D0%B2%D0%B8%D1%87&amp;action=info" title="Подробнее об этой странице">Сведения о странице</a></li><li id="t-cite"><a href="/index.php?title=%D0%A1%D0%BB%D1%83%D0%B6%D0%B5%D0%B1%D0%BD%D0%B0%D1%8F:%D0%A6%D0%B8%D1%82%D0%B0%D1%82%D0%B0&amp;page=%D0%92%D0%BB%D0%B0%D1%81%D0%BE%D0%B2%2C_%D0%92%D0%B0%D0%BB%D0%B5%D0%BD%D1%82%D0%B8%D0%BD_%D0%A1%D1%82%D0%B5%D0%BF%D0%B0%D0%BD%D0%BE%D0%B2%D0%B8%D1%87&amp;id=20724" title="Информация о том, как цитировать эту страницу">Цитировать страницу</a></li>					</ul>
							</div>
		</div>
				</div>
		</div>
		<div id="footer" role="contentinfo">
							<ul id="footer-info">
											<li id="footer-info-lastmod"> Эта страница последний раз была отредактирована 20 марта 2022 в 16:20.</li>
									</ul>
							<ul id="footer-places">
											<li id="footer-places-privacy"><a href="/index.php?title=FA100:%D0%9F%D0%BE%D0%BB%D0%B8%D1%82%D0%B8%D0%BA%D0%B0_%D0%BA%D0%BE%D0%BD%D1%84%D0%B8%D0%B4%D0%B5%D0%BD%D1%86%D0%B8%D0%B0%D0%BB%D1%8C%D0%BD%D0%BE%D1%81%D1%82%D0%B8" title="FA100:Политика конфиденциальности">Политика конфиденциальности</a></li>
											<li id="footer-places-about"><a href="/index.php?title=FA100:%D0%9E%D0%BF%D0%B8%D1%81%D0%B0%D0%BD%D0%B8%D0%B5" title="FA100:Описание">Описание FA100</a></li>
											<li id="footer-places-disclaimer"><a href="/index.php?title=FA100:%D0%9E%D1%82%D0%BA%D0%B0%D0%B7_%D0%BE%D1%82_%D0%BE%D1%82%D0%B2%D0%B5%D1%82%D1%81%D1%82%D0%B2%D0%B5%D0%BD%D0%BD%D0%BE%D1%81%D1%82%D0%B8" title="FA100:Отказ от ответственности">Отказ от ответственности</a></li>
									</ul>
										<ul id="footer-icons" class="noprint">
											<li id="footer-poweredbyico">
							<a href="//www.mediawiki.org/"><img src="/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" srcset="/resources/assets/poweredby_mediawiki_132x47.png 1.5x, /resources/assets/poweredby_mediawiki_176x62.png 2x" width="88" height="31"/></a>						</li>
									</ul>
						<div style="clear:both"></div>
		</div>
		<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgPageParseReport":{"limitreport":{"cputime":"0.005","walltime":"0.005","ppvisitednodes":{"value":1,"limit":1000000},"ppgeneratednodes":{"value":4,"limit":1000000},"postexpandincludesize":{"value":0,"limit":2097152},"templateargumentsize":{"value":0,"limit":2097152},"expansiondepth":{"value":1,"limit":40},"expensivefunctioncount":{"value":0,"limit":100},"timingprofile":["100.00%    0.000      1 -total"]},"cachereport":{"timestamp":"20240404080851","ttl":86400,"transientcontent":false}}});});</script><script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgBackendResponseTime":112});});</script>
	</body>
</html>
