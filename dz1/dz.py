import asyncio
from typing import List
import shutil
import os
import aiofiles
from http_client import HttpAdapter


async def parse(http_adapter: HttpAdapter, url: str, file_number: int) -> None:
    print(f'parse link: {url}')
    text = await http_adapter.http_session(
                method="GET",
                url=url,
            )

    try:
        async with aiofiles.open(COMMON_RESULTS_FILE, mode='a', encoding="utf-8") as f:
            await f.write(text)
        async with aiofiles.open(os.path.join(SCRAPED_FOLDER, f'{file_number}.txt'), mode='w', encoding="utf-8") as f:
            await f.write(text)
    except Exception as e:
        print(e)


def delete_files_in_folder(folder_path) -> None:
    shutil.rmtree(folder_path)
    os.mkdir(folder_path)


async def main():
    http_adapter = HttpAdapter()
    delete_files_in_folder(SCRAPED_FOLDER)
    links: List[str] = []
    async with aiofiles.open(START_LINKS_FILE, mode='r', encoding="utf-8") as f:
        async for line in f:
            links.append(line)

    async with aiofiles.open(INDEXED_LINKS_FILE, mode='w', encoding="utf-8") as f:
        for i, link in enumerate(links):
            await f.write(f'{i}, {link}')

    # print(links)
    print(f'links count {len(links)}')
    await asyncio.gather(*[parse(http_adapter, link, index) for index, link in enumerate(links)])


if __name__ == '__main__':
    START_LINKS_FILE = 'links.txt'
    INDEXED_LINKS_FILE = 'index.txt'
    SCRAPED_FOLDER = 'scraped'
    COMMON_RESULTS_FILE = 'выкачка.txt'
    asyncio.run(main())
