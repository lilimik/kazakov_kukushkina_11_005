import asyncio
import os
from collections import defaultdict
from typing import List, Dict, Set

from dz3.bool_search import BoolSearcher


def check_file(
        file: str,
        lem_tokens: Dict[str, List[str]],
        file_number: int,
        result: Dict[str, Set[int]]
):
    for key, values in lem_tokens.items():
        for value in values:
            if value in file:
                result[key].add(file_number)


def check_filename(filename: str) -> str | None:
    file_number = filename[:-4]
    return file_number if filename.endswith('.txt') and file_number.isdigit() else None


async def main():
    lem_tokens: Dict[str, List[str]] = defaultdict(list)
    data: Dict[str, Set[int]] = defaultdict(set)

    with open(LEMMATIZED_TOKENS_FILE, mode='r', encoding=FILE_ENCODING) as f:
        line = f.readline()
        while line:
            key, values = line.split(' ', 1)
            values = values.replace('\n', '').split(' ')
            lem_tokens[key] += values
            line = f.readline()

    for filename in os.listdir(SCRAPED_RESULTS_FOLDER):
        if file_number := check_filename(filename):
            file_path = os.path.join(SCRAPED_RESULTS_FOLDER, filename)
            with open(file_path, mode='r', encoding="utf-8") as f:
                check_file(f.read(), lem_tokens, int(file_number), data)

    with open(INVERTED_LEMMATIZED_TOKENS_FILE, mode='w', encoding="utf-8") as f:
        for key, values in data.items():
            f.writelines(f'{key} {values}\n')

    # Пример использования:
    query = "(провести AND увековечивать) OR ненужный"
    result = BoolSearcher(query, data).bool_search()
    print(result)


if __name__ == '__main__':
    INVERTED_LEMMATIZED_TOKENS_FILE = 'inverted_lemmatized_tokens'
    FILE_ENCODING = 'utf-8'
    LEMMATIZED_TOKENS_FILE = '../dz2/lemmatized_tokens.txt'
    SCRAPED_RESULTS_FOLDER = '../dz1/scraped'
    COMMON_RESULTS_FILE = '../dz1/выкачка.txt'
    asyncio.run(main())
